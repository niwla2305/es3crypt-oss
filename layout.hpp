#ifndef _LAYOUT_HPP
#include <string>
#include <vector>


#ifdef ES3CRYPT_WEB_LAYOUT
struct Layout {
    struct Entry {
        std::string key, type, default_value;
        bool hidden = false;
        std::string extra_info;
    };
    enum Format {
        JSON // Only that is currently supported
    };

    std::string game_name,
                password;
    size_t buffer_size;
    Format format;
    std::string html_head,
                html_footer;
    std::vector<Entry> entries;
};

static const Layout layout =
#   include ES3CRYPT_WEB_LAYOUT
;
#endif
#endif
