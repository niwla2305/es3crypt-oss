/*
    es3crypt-oss
    Copyright (C) 2022  niansa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "es3crypt.hpp"
#include "crow_all.h"
#include "layout.hpp"

#include <string>
#include <vector>
#include <sstream>



int main() {
    crow::SimpleApp app;

    CROW_ROUTE(app, "/").methods("GET"_method)([] () {
        // Static content
#       ifdef ES3CRYPT_WEB_LAYOUT
        static auto cached = [] () {
            std::string resp = R"(
                <!DOCTYPE html>
                <html>
                    <head>
                        <title>ES3Crypt Web frontend for )" + layout.game_name + R"(</title>
                    </head>
                    <body>
                        <h1>ES3Crypt for )" + layout.game_name + R"(</h1>)";
            resp += layout.html_head;
            resp += "<form action='/SaveFile.es3' method='get'>";
            for (const auto& entry : layout.entries) {
                resp += "<div ";
                if (entry.hidden) {
                    resp += "hidden";
                }
                resp += "><p>" + entry.key;
                if (!entry.extra_info.empty()) {
                    resp += " <i>" + entry.extra_info + "</i>";
                }
                resp += "</p><input type='";
                if (entry.type == "int") {
                    resp += "number";
                } else {
                    resp += "text";
                }
                resp += "' name='";
                resp += entry.key;
                resp += "' value='";
                resp += entry.default_value;
                resp += "'></input></div><br/>";
            }
            resp += R"(
                            <br />
                            <input type="submit" value="Download"></input>
                        </form>
                        <br/><br/>)";
            resp += layout.html_footer;
            resp += R"(
                    </body>
                </html>
            )";
            return resp;
        }();
        return cached;
#       else
        return R"(
            <!DOCTYPE html>
            <html>
                <head>
                    <title>ES3Crypt Web frontend</title>
                </head>
                <body>
                    <h1>ES3Crypt for Phasmophobia</h1>
                    <form action="/SaveFile.es3" method="get">
                        <p>Data:</p>
                        <textarea name="data">Enter content here...</textarea>
                        <p>Password:</p>
                        <input type="text" name="password"></input>
                        <p>Buffer size:</p>
                        <input type="number" name="buffer_size"></input>
                        <input type="submit" value="Download"></input>
                    </form>
                </body>
            </html>
        )";
#       endif
    });

    CROW_ROUTE(app, "/SaveFile.es3").methods("GET"_method)([] (const crow::request& req) {
#ifdef  ES3CRYPT_WEB_LAYOUT
        // Generate json data
        std::stringstream raw_json;
        {
            crow::json::wvalue json;
            for (const auto& entry : layout.entries) {
                auto value = req.url_params.get(entry.key);
                if (value) {
                    auto& json_key = json[entry.key];
                    if (entry.type == "int") {
                        json_key["value"] = std::stoi(value);
                    } else {
                        json_key["value"] = std::string(value);
                    }
                    json_key["__type"] = entry.type;
            }
            }
            raw_json << json.dump();
        }
        // Encrypt it all
        std::ostringstream encrypted;
        while (raw_json) {
            ES3Crypt::encrypt(raw_json, encrypted, layout.password, layout.buffer_size);
        }
#       else
        std::string data = req.url_params.get("data"),
                    password = req.url_params.get("password"),
                    buffer_size = req.url_params.get("buffer_size");
        std::istringstream data_stream(data);
        std::ostringstream encrypted;
        while (data_stream) {
            ES3Crypt::encrypt(data_stream, encrypted, password, std::stoi(buffer_size));
        }
#       endif
        // Return encrypted data as file response
        crow::response resp;
        resp.set_header("Content-Type", "application/octet-stream");
        resp.write(encrypted.str());
        return resp;
    });

    app.port(8048).multithreaded().run();
}
